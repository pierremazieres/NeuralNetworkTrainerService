FROM ubuntu:latest
LABEL maintener="ymir mazieres.pierre@gmail.com"

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y python3 python3-coverage python3-psutil python3-aniso8601 python3-testresources libpq-dev python3-flask-restful python3-numpy python3-psycopg2
RUN ln -s /usr/local/lib/python3.* /usr/local/lib/python3
RUN rm -Rf /tmp/work
RUN mkdir -p /tmp/work
COPY PythonCommonTools-0.0.2.linux-x86_64.tar.gz /tmp/work
COPY NeuralNetworkCommon-0.0.2.linux-x86_64.tar.gz /tmp/work
COPY NeuralNetworkTrainerService-0.0.3.linux-x86_64.tar.gz /tmp/work
RUN find /tmp/work -name "*.gz" -exec tar -xzf {} -C /tmp/work \;
RUN cp -R /tmp/work/usr/local/lib/python3.*/dist-packages/* /usr/local/lib/python3/dist-packages/
RUN rm -Rf /tmp/work

EXPOSE 5000

CMD python3 /usr/local/lib/python3/dist-packages/neuralnetworktrainerservice/neuralNetworkTrainerServiceDaemon.py start