#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.perceptron import selectById as perceptronSelectById
from neuralnetworkcommon.database.layer import deleteAllByPerceptronId
from neuralnetworkcommon.database.trainingSession import selectByPerceptronId, deleteByPerceptronId as trainingSessionDeleteByPerceptronId
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter, deleteByPerceptronId as trainingSessionElementDeleteByPerceptronId
from random import randint
from testneuralnetworktrainerservice.commonUtilities import CommonTest, setRandomPerceptronTrainingSetSession, prepareTrainer
from neuralnetworkcommon.database.trainingElement import update, deleteAllByTrainingSetId, selectById as trainingElementSelectById
# test trainer
def analyseTestData(input,perceptron):
    actualOutput = perceptron.layerTrainingDB.passForward(input)
    actualOutput = [float(_) for _ in actualOutput]
    maximumValue = max(actualOutput)
    maximumValueIndex = actualOutput.index(maximumValue)
    minimumValue = min(actualOutput)
    return actualOutput, maximumValueIndex, minimumValue, maximumValue
def generateDispatchedError(trainingElementIds, perceptron):
    expectedErrorNumber = randint(1, len(trainingElementIds))
    errorCounter = 0
    for trainingElementId in trainingElementIds:
        trainingElement = trainingElementSelectById(CommonTest.cursor,trainingElementId)
        actualOutput, maximumValueIndex, minimumValue, maximumValue = analyseTestData(trainingElement.input,perceptron)
        # generate error (if not enought yet)
        if errorCounter < expectedErrorNumber:
            actualOutput[maximumValueIndex] = minimumValue - 1
            errorCounter += 1
        else:
            actualOutput[maximumValueIndex] = maximumValue + 1
        trainingElement.expectedOutput = actualOutput
        update(CommonTest.cursor,trainingElement)
    return errorCounter
def generateErrorEverywhere(trainingElementIds, perceptron):
    for trainingElementId in trainingElementIds:
        trainingElement = trainingElementSelectById(CommonTest.cursor,trainingElementId)
        _, maximumValueIndex, minimumValue, _ = analyseTestData(trainingElement.input,perceptron)
        trainingElement.expectedOutput[maximumValueIndex] = minimumValue - 1
        update(CommonTest.cursor,trainingElement)
def cleanTrainer(trainer):
    trainer.connection.commit()
    trainer.trainerDB.cleanup()
    trainer.cursor.close()
    trainer.connection.close()
    pass
class testTrainerDB(CommonTest):
    def testComputeTestScore(self):
        # prepare trainer
        trainer, _, _ = prepareTrainer(testScore=1)
        # check initial test score
        fetchedInitialSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedInitialSession.testScore,"ERROR : initial testScore does not match")
        # compute test score
        trainer.trainerDB.computeTestScore()
        cleanTrainer(trainer)
        # check updated test score
        fetchedUpdatedSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertGreaterEqual(fetchedUpdatedSession.testScore,0,"ERROR : updated testScore negative")
        self.assertLessEqual(fetchedUpdatedSession.testScore,1,"ERROR : updated testScore greater than one")
        pass
    def testGenerateTrainingSubSequence(self):
        # initialize training session & trainer
        trainer = setRandomPerceptronTrainingSetSession()
        trainer.intervalCounter = 1
        trainingElementsNumber = len(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId, testFilter=False))
        # train subsequence
        errorElementsNumber = trainer.trainerDB.generateTrainingSubSequence()
        cleanTrainer(trainer)
        # check training
        self.assertGreaterEqual(errorElementsNumber, 0, "ERROR : negative errorElementsNumber")
        self.assertLessEqual(errorElementsNumber, trainingElementsNumber, "ERROR : errorElementsNumber greater than training set")
        pass
    def testTrainerErrorsMethods(self):
        for expectedPartiallyTrained in {True,False}:
            # randomize perceptron and training set
            trainer = setRandomPerceptronTrainingSetSession()
            perceptron = perceptronSelectById(CommonTest.cursor,CommonTest.testPerceptronId,True)
            trainingElementIds = selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId, testFilter=False)
            trainingElementsNumber = len(trainingElementIds)
            trainer.trainingChunkSize = trainingElementsNumber
            # generate errors
            if expectedPartiallyTrained:
                expectedErrorNumber = generateDispatchedError(trainingElementIds, perceptron)
            else:
                expectedErrorNumber = trainingElementsNumber
                generateErrorEverywhere(trainingElementIds, perceptron)
            CommonTest.connection.commit()
            # run training checking
            actualErrorNumber = trainer.trainerDB.countErrorElements()
            actualErrorElementsNumber = trainer.trainerDB.generateTrainingSubSequence()
            cleanTrainer(trainer)
            # check training checking
            self.assertEqual(actualErrorNumber, expectedErrorNumber, "ERROR : errorNumber does not match")
            self.assertLessEqual(actualErrorElementsNumber,trainingElementsNumber, "ERROR : errorElements number does not match")
            # clean training elements
            trainingSessionElementDeleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
            trainingSessionDeleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
            deleteAllByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
            deleteAllByTrainingSetId(CommonTest.cursor,CommonTest.testTrainingSetId)
            pass
    def testPassForwardBackwardSequenceAndPartiallyTrained(self):
        # randomize perceptron and training set
        trainer = setRandomPerceptronTrainingSetSession()
        # test pass forward/backward
        meanDifferentialError = trainer.trainerDB.passForwardBackwardSequence()
        partiallyTrained = trainer.trainerDB.isPartiallyTrained()
        trainer.trainerDB.cleanup()
        # check pass forward/backward
        self.assertIsNotNone(meanDifferentialError, "ERROR : pass forward/back for subsequence does not match")
        self.assertTrue(partiallyTrained,"ERROR : partiallyTrained does not match")
        pass
    pass
pass
