#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.trainingSession import selectByPerceptronId as trainingSessionSelectByPerceptronId
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter
from testneuralnetworktrainerservice.commonUtilities import CommonTest, setRandomPerceptronTrainingSetSession, prepareTrainer
from neuralnetworkcommon.database.trainingSessionProgress import selectByPerceptronId as trainingSessionProgressSelectByPerceptronId
# test trainer
class testTrainer(CommonTest):
    def testRunWithError(self):
        # prepare trainer
        # INFO : we set test score on 0 in order to a "divide by 0" error
        trainer, _, pid = prepareTrainer()
        # check initial data
        fetchedInitialSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedInitialSession.pid,pid,"ERROR : initial with error PID does not match")
        self.assertIsNone(fetchedInitialSession.errorMessage,"ERROR : initial with error errorMessage does not match")
        runError = True
        try:
            # run trainer
            trainer.run()
            runError = False
        except:
            # check training result
            fetchedInitialSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
            self.assertIsNone(fetchedInitialSession.pid,"ERROR : final with error PID does not match")
            self.assertIsNotNone(fetchedInitialSession.errorMessage,"ERROR : final with error errorMessage does not match")
        finally:
            self.assertTrue(runError,"ERROR : run did not failed")
    def testRunWithoutError(self):
        # prepare trainer
        trainer, _, pid = prepareTrainer(testScore=0.9)
        # check initial data
        fetchedInitialSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedInitialSession.pid,pid,"ERROR : initial without error PID does not match")
        self.assertIsNone(fetchedInitialSession.errorMessage,"ERROR : initial without error errorMessage does not match")
        fetchedInitialSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedInitialSession.testScore,"ERROR : initial without error testScore does not match")
        self.assertIsNone(fetchedInitialSessionProgress,"ERROR : initial without error training session progress does not match")
        # run trainer
        trainer.run()
        # check training result
        fetchedFinalSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedFinalSession.pid,"ERROR : final without error PID does not match")
        self.assertIsNone(fetchedFinalSession.errorMessage,"ERROR : final without error errorMessage does not match")
        self.assertIsNotNone(fetchedFinalSession.testScore,"ERROR : final without error testScore does not match")
        fetchedFinalSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(fetchedFinalSessionProgress.meanDifferentialErrors,"ERROR : final without error meanDifferentialError does not match")
        self.assertIsNotNone(fetchedFinalSessionProgress.errorElementsNumbers,"ERROR : final without error errorElementsNumber does not match")
        self.assertIsNotNone(fetchedFinalSessionProgress.resets,"ERROR : final without error errorElementsNumber does not match")
    def testTrainSubSequence(self):
        # initialize training session & trainer
        trainer = setRandomPerceptronTrainingSetSession()
        trainer.intervalCounter = 1
        trainingElementsNumber = len(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId, testFilter=False))
        # train subsequence
        trainer.trainerDB.generateTrainingSubSequence()
        trainer.trainerDB.cleanup()
        meanDifferentialError, errorElementsNumber, reset = trainer.trainSubSequence()
        # check training
        self.assertEqual(meanDifferentialError, 0, "ERROR : meanDifferentialError does not match")
        self.assertGreaterEqual(errorElementsNumber, 0, "ERROR : negative errorElementsNumber")
        self.assertLessEqual(errorElementsNumber, trainingElementsNumber, "ERROR : errorElementsNumber greater than training set")
        self.assertTrue(reset, "ERROR : reset does not match")
        pass
    def testTrainCompleteSequenceWithReset(self):
        # train completly
        trainer, initialTrainingSession, _ = prepareTrainer(maximumTry=0)
        reset = trainer.trainCompleteSequence()
        trainer.trainerDB.cleanup()
        # check complete training
        self.assertTrue(reset, "Error reset not triggered")
        fetchedTrainingSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(initialTrainingSession.perceptronId,fetchedTrainingSession.perceptronId,"ERROR : training session perceptronId does not match")
        self.assertEqual(initialTrainingSession.trainingSetId,fetchedTrainingSession.trainingSetId,"ERROR : training session trainingSetId does not match")
        fetchedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertGreaterEqual(fetchedTrainingSessionProgress.errorElementsNumbers[-1],0,"ERROR : errorElementsNumbers does not match")
        self.assertEqual(len(fetchedTrainingSessionProgress.errorElementsNumbers),len(fetchedTrainingSessionProgress.meanDifferentialErrors),"ERROR : errorElementsNumbers/meanDifferentialErrors number does not match")
        self.assertEqual(fetchedTrainingSessionProgress.resets[-1],True,"ERROR : resets number does not match")
        '''INFO :
         - trained elements number is not always ascending
         - mean errors is not always descending
        '''
        pass
    def testTrainCompleteSequenceWithoutReset(self):
        # train completly
        trainer, initialTrainingSession, _ = prepareTrainer(testScore=0.9)
        reset= trainer.trainCompleteSequence()
        trainer.trainerDB.cleanup()
        # check complete training
        self.assertFalse(reset, "Error unexpected reset")
        fetchedTrainingSession = trainingSessionSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(initialTrainingSession.perceptronId,fetchedTrainingSession.perceptronId,"ERROR : training session perceptronId does not match")
        self.assertEqual(initialTrainingSession.trainingSetId,fetchedTrainingSession.trainingSetId,"ERROR : training session trainingSetId does not match")
        fetchedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedTrainingSessionProgress.errorElementsNumbers[-1],0,"ERROR : errorElementsNumbers does not match")
        self.assertEqual(len(fetchedTrainingSessionProgress.errorElementsNumbers),len(fetchedTrainingSessionProgress.meanDifferentialErrors),"ERROR : errorElementsNumbers/meanDifferentialErrors number does not match")
        self.assertEqual(set(fetchedTrainingSessionProgress.resets),{False},"ERROR : resets number does not match")
        '''INFO :
         - trained elements number is not always ascending
         - mean errors is not always descending
        '''
        pass
    pass
pass
