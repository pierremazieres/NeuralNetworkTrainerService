#!/usr/bin/env python3
# coding=utf-8
# import
from testneuralnetworkcommon.commonUtilities import randomTrainingElement, insertRandomTrainingElements, CommonTest, randomTrainingSession, randomizeTrainingSessionElementsError
from neuralnetworktrainerservice.service.service import endpoint
from neuralnetworkcommon.entity.trainingElement import TrainingElement
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from gzip import compress, decompress
from json import loads
from neuralnetworkcommon.database.trainingElement import selectById
from neuralnetworkcommon.database.trainingSession import insert,deleteByPerceptronId
from random import random
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworktrainerservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalTrainingElementUrl, specificTrainingElementUrl
# test training element
compressedBadTrainingElement = compress(TrainingElement('', -1, [], []).dumpToSimpleJson().encode())
globalResource = globalTrainingElementUrl(endpoint)
class testTrainingElementWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # initialize training element
        rawInitialTrainingElement = randomTrainingElement(trainingSetId=CommonTest.testTrainingSetId)
        CommonTest.connection.commit()
        # precheck
        self.assertIsNone(rawInitialTrainingElement.id,"ERROR : trainingElement has id")
        # create training element
        dumpedInitiaTrainingElement = rawInitialTrainingElement.dumpToSimpleJson()
        compressedInitiaTrainingElement = compress(dumpedInitiaTrainingElement.encode())
        response = clientApplication.post(globalResource,data=compressedInitiaTrainingElement)
        trainingElementId = int(decompress(response.data))
        # check creation
        self.assertIsNotNone(trainingElementId,"ERROR : trainingSet has no id")
        rawInitialTrainingElement.id = trainingElementId
        specificResource = specificTrainingElementUrl(endpoint,trainingElementId)
        # read training element
        response = clientApplication.get(specificResource)
        jsonTrainingElement = loads(decompress(response.data).decode())
        fetchedInsertedTrainingElement = loadFromDict(jsonTrainingElement)
        # check reading
        self.assertEqual(rawInitialTrainingElement,fetchedInsertedTrainingElement,"ERROR : inserted trainingElement does not match")
        # update training element
        rawNewTrainingElement = randomTrainingElement(trainingElementId,CommonTest.testTrainingSetId)
        rawNewTrainingElement.id = trainingElementId
        dumpedNewTrainingElement = rawNewTrainingElement.dumpToSimpleJson()
        compressedNewTrainingElement = compress(dumpedNewTrainingElement.encode())
        clientApplication.put(globalResource,data=compressedNewTrainingElement)
        # check update
        response = clientApplication.get(specificResource)
        jsonTrainingElement = loads(decompress(response.data).decode())
        fetchedUpdatedTrainingElement = loadFromDict(jsonTrainingElement)
        self.assertNotEqual(fetchedUpdatedTrainingElement,fetchedInsertedTrainingElement,"ERROR : trainingElement not updated")
        self.assertEqual(fetchedUpdatedTrainingElement,rawNewTrainingElement,"ERROR : inserted trainingElement does not match")
        # call DB delete
        clientApplication.delete(specificResource)
        # check DB delete
        response = clientApplication.get(specificResource)
        jsonTrainingElement = loads(decompress(response.data).decode())
        self.assertIsNone(jsonTrainingElement,"ERROR : trainingElement not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training elements
        trainingSession = randomTrainingSession(CommonTest.testPerceptronId, CommonTest.testTrainingSetId)
        initialIds = set(insertRandomTrainingElements(trainingSetId=CommonTest.testTrainingSetId))
        insert(CommonTest.cursor,trainingSession,random())
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        # select IDs from training set
        globalResource_TrainingSetId = globalTrainingElementUrl(endpoint,CommonTest.testTrainingSetId)
        response = clientApplication.get(globalResource_TrainingSetId)
        trainingSet_fetchedIds = loads(decompress(response.data).decode())
        trainingSet_expectedFirstId = trainingSet_fetchedIds[0]
        trainingSet_fetchedIds = set(trainingSet_fetchedIds)
        firstElement = selectById(CommonTest.cursor,trainingSet_expectedFirstId)
        trainingResource =  globalTrainingElementUrl(endpoint,CommonTest.testTrainingSetId,expectedOutput=firstElement.expectedOutput)
        response = clientApplication.get(trainingResource)
        trainingSet_fetchedIds_ExpectedOutput = set(loads(decompress(response.data).decode()))
        # check IDs from training set
        self.assertTrue(initialIds.issubset(trainingSet_fetchedIds),"ERROR : IDs selection does not match")
        self.assertSetEqual(trainingSet_fetchedIds_ExpectedOutput, {trainingSet_expectedFirstId},msg="ERROR : trainingSet first ID does not match")
        # select IDs from perceptron
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=False)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_training = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=True)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_test = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,errorFilter=False)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_fine = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,errorFilter=True)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_error = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=False,errorFilter=False)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_trainingFine = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=False,errorFilter=True)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_trainingError = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=True,errorFilter=False)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_testFine = set(loads(decompress(response.data).decode()))
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,testFilter=True,errorFilter=True)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_testError = set(loads(decompress(response.data).decode()))
        firstElement = selectById(CommonTest.cursor,trainingSet_expectedFirstId)
        trainingResource = globalTrainingElementUrl(endpoint,perceptronId=CommonTest.testPerceptronId,expectedOutput=firstElement.expectedOutput)
        response = clientApplication.get(trainingResource)
        perceptron_fetchedIds_ExpectedOutput = set(loads(decompress(response.data).decode()))
        # check IDs from perceptron
        self.assertSetEqual(perceptron_fetchedIds_trainingFine.union(perceptron_fetchedIds_trainingError), perceptron_fetchedIds_training,msg="ERROR : training IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_testFine.union(perceptron_fetchedIds_testError), perceptron_fetchedIds_test,msg="ERROR : test IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_trainingFine.union(perceptron_fetchedIds_testFine), perceptron_fetchedIds_fine,msg="ERROR : fine IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_trainingError.union(perceptron_fetchedIds_testError), perceptron_fetchedIds_error,msg="ERROR : error IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_training.union(perceptron_fetchedIds_test), trainingSet_fetchedIds,msg="ERROR : training + test IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_fine.union(perceptron_fetchedIds_error), trainingSet_fetchedIds,msg="ERROR : error + fine IDs does not match")
        self.assertSetEqual(perceptron_fetchedIds_ExpectedOutput, {trainingSet_expectedFirstId},msg="ERROR : perceptron first ID does not match")
        # cross check
        self.assertEqual(trainingSet_fetchedIds,trainingSet_fetchedIds,"ERROR : elements IDs does not match")
        # delete all
        deleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        clientApplication.delete(globalResource_TrainingSetId)
        # check IDs
        response = clientApplication.get(globalResource_TrainingSetId)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post, globalResource, compressedBadTrainingElement)
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put, globalResource, compressedBadTrainingElement)
    pass
pass
