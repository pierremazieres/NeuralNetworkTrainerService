# coding=utf-8
# import
from testneuralnetworktrainerservice.commonUtilities import CommonTest
from pythoncommontools.service.service import application
# services test utilities
clientApplication = application.test_client()
class TestService(CommonTest):
    @classmethod
    def setUpClass(cls):
        CommonTest.setUpClass()
        clientApplication.testing = True
    pass
pass
