#!/usr/bin/env python3
# coding=utf-8
# import
from testneuralnetworktrainerservice.commonUtilities import CommonTest, insertRandomTrainingElements
from neuralnetworkcommon.service.service import TrainerAction
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from neuralnetworktrainerservice.service.service import endpoint
from neuralnetworkcommon.database.trainingSession import selectByPerceptronId,insert,updatePid
from gzip import decompress
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from testneuralnetworktrainerservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import executeTrainerUrl
# test training session
class testTrainerWS(TestService):
    # test actions OK
    def testActionsOK(self):
        # randomize initial training session
        insertRandomTrainingElements(CommonTest.testTrainingSetId)
        initialTrainingSession = TrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId)
        insert(CommonTest.cursor,initialTrainingSession,0)
        CommonTest.connection.commit()
        # precheck
        fetchedNoExistingTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedNoExistingTrainingSession.pid,"ERROR : initialized with PID value")
        self.assertIsNone(fetchedNoExistingTrainingSession.errorMessage,"ERROR : initialized with errorMessage value")
        # start
        startUrl = executeTrainerUrl(endpoint,CommonTest.testPerceptronId,TrainerAction.START.value)
        response = clientApplication.get(startUrl)
        # check start
        firstPid = int(decompress(response.data).strip())
        self.assertIsNotNone(firstPid,"ERROR : PID not set on start")
        fetchedStartedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        sessionErrorMessage = fetchedStartedTrainingSession.errorMessage
        started = fetchedStartedTrainingSession.pid  or sessionErrorMessage
        self.assertTrue(started,"ERROR : trainer did not start")
        # stop
        stopUrl = executeTrainerUrl(endpoint,CommonTest.testPerceptronId,TrainerAction.STOP.value)
        clientApplication.get(stopUrl)
        # check stop
        fetchedStoppedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedStoppedTrainingSession.pid,"ERROR : stopped with PID value")
        self.assertEqual(fetchedStoppedTrainingSession.errorMessage,sessionErrorMessage,"ERROR : stopped with new errorMessage")
        # simulate crashed trainer : a PID is register in database but there is no associated process running
        updatePid(CommonTest.cursor,CommonTest.testPerceptronId, firstPid)
        CommonTest.connection.commit()
        response = clientApplication.get(startUrl)
        # check crashed trainer restart
        newPid = int(decompress(response.data).strip())
        self.assertNotEqual(newPid,firstPid,"ERROR : crashed trainer did not restart")
        fetchedRestartedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        sessionErrorMessage = fetchedRestartedTrainingSession.errorMessage
        self.assertEqual(sessionErrorMessage , fetchedRestartedTrainingSession.errorMessage,"ERROR : trainer did not restart")
        # stop crashed trainer
        clientApplication.get(stopUrl)
    def testInvalidAction(self):
        specificResource = executeTrainerUrl(endpoint,CommonTest.testPerceptronId,"idle")
        checkRestServerNotDefaultError(self,clientApplication.get,specificResource)
    def testNoAction(self):
        specificResource = executeTrainerUrl(endpoint,CommonTest.testPerceptronId)
        checkRestServerNotDefaultError(self,clientApplication.get,specificResource)
    pass
pass
