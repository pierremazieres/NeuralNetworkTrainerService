#!/usr/bin/env python3
# coding=utf-8
# import
from random import randint, random, choice, uniform
from string import ascii_letters
from neuralnetworktrainerservice.service.service import endpoint
from json import loads
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from neuralnetworkcommon.database.perceptron import deleteById as perceptronDeleteById, deleteAllByWorkspaceId as perceptronDeleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingSet import deleteById as trainingSetDeleteById, deleteAllByWorkspaceId as trainingSetDeleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingSession import insert
from neuralnetworkcommon.database.trainingSessionProgress import selectByPerceptronId, deleteByPerceptronId, insert as trainingSessionProgressInsert
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter
from gzip import compress, decompress
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from testneuralnetworkcommon.commonUtilities import CommonTest, randomTrainingSession, randomizeTrainingSessionElementsError, randomTrainingSessionProgress, insertRandomTrainingSessions
from neuralnetworkcommon.database.trainingElement import deleteAllByTrainingSetId
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworktrainerservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalTrainingSessionUrl, specificTrainingSessionUrl, trainingSessionSummaryUrl
# test training session
globalResource = globalTrainingSessionUrl(endpoint)
testRatio = random()
globalResourceRatio =  globalTrainingSessionUrl(endpoint,testRatio=testRatio)
compressedBadTrainingSession = compress(TrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId,1).dumpToSimpleJson().encode())
class testTrainingSessionWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random perceptron & training set
        rawInitiaTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId,None,None,None)
        CommonTest.connection.commit()
        dumpedInitiaTrainingSession = rawInitiaTrainingSession.dumpToSimpleJson()
        compressedInitiaTrainingSession = compress(dumpedInitiaTrainingSession.encode())
        response = clientApplication.post(globalResourceRatio,data=compressedInitiaTrainingSession)
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        # check creation
        self.assertEqual(response.status_code,200,"ERROR : training session was not created")
        # read training session
        specificResource = specificTrainingSessionUrl(endpoint,CommonTest.testPerceptronId)
        response = clientApplication.get(specificResource)
        jsonTrainingSession = loads(decompress(response.data).decode())
        fetchedInsertedTrainingSession = loadFromDict(jsonTrainingSession)
        # check reading
        self.assertEqual(rawInitiaTrainingSession,fetchedInsertedTrainingSession,"ERROR : inserted trainingSession does not match")
        # insert training progress
        trainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId, randint(1000, 9999))
        trainingSessionProgressInsert(CommonTest.cursor,trainingSessionProgress)
        insertedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(insertedTrainingSessionProgress, "ERROR : trainingSessionProgress not inserted")
        # update training session
        rawNewTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId)
        dumpedNewTrainingSession = rawNewTrainingSession.dumpToSimpleJson()
        compressedNewTrainingSession = compress(dumpedNewTrainingSession.encode())
        clientApplication.put(globalResourceRatio,data=compressedNewTrainingSession)
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        # check update
        response = clientApplication.get(specificResource)
        jsonTrainingSession = loads(decompress(response.data).decode())
        fetchedUpdatedTrainingSession = loadFromDict(jsonTrainingSession)
        self.assertNotEqual(fetchedUpdatedTrainingSession,fetchedInsertedTrainingSession,"ERROR : training session not updated")
        self.assertEqual(fetchedUpdatedTrainingSession,rawNewTrainingSession,"ERROR : updated trainingSession does not match")
        updatedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(updatedTrainingSessionProgress, "ERROR : trainingSessionProgress not deleted")
        # summarize training session
        summaryResource = trainingSessionSummaryUrl(endpoint,CommonTest.testPerceptronId)
        response = clientApplication.get(summaryResource)
        jsonTrainingSessionSummary = loads(decompress(response.data).decode())
        trainingSessionSummary = loadFromDict(jsonTrainingSessionSummary)
        # check summary
        saveInterval = fetchedUpdatedTrainingSession.saveInterval
        fetchedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        updated_allIdsByPerceptronId_fineTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=False));
        updated_allIdsByPerceptronId_errorTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=True));
        updated_allIdsByPerceptronId_fineTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=False));
        updated_allIdsByPerceptronId_errorTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=True));
        self.assertEqual(trainingSessionSummary.perceptronId,fetchedUpdatedTrainingSession.perceptronId,"ERROR : trainingSession summary perceptronId does not match")
        self.assertEqual(trainingSessionSummary.trainingSetId,fetchedUpdatedTrainingSession.trainingSetId,"ERROR : trainingSession summary trainingSetId does not match")
        self.assertEqual(trainingSessionSummary.saveInterval,saveInterval,"ERROR : trainingSession summary saveInterval does not match")
        self.assertEqual(trainingSessionSummary.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : trainingSession summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.maximumErrorRatio,fetchedUpdatedTrainingSession.maximumErrorRatio,"ERROR : trainingSession summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTraining,len(updated_allIdsByPerceptronId_fineTraining),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTraining,len(updated_allIdsByPerceptronId_errorTraining),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTest,len(updated_allIdsByPerceptronId_fineTest),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTest,len(updated_allIdsByPerceptronId_errorTest),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.pid,fetchedUpdatedTrainingSession.pid,"ERROR : trainingSession summary pid does not match")
        self.assertEqual(trainingSessionSummary.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : trainingSession summary errorMessage does not match")
        self.assertEqual(trainingSessionSummary.progressRecordsNumber,len(fetchedTrainingSessionProgress.meanDifferentialErrors),"ERROR : trainingSession summary errorMessage does not match")
        self.assertEqual(trainingSessionSummary.meanDifferentialErrors,fetchedTrainingSessionProgress.meanDifferentialErrors[-1],"ERROR : trainingSession summary meanDifferentialError does not match")
        self.assertEqual(trainingSessionSummary.errorElementsNumbers,fetchedTrainingSessionProgress.errorElementsNumbers[-1],"ERROR : trainingSession summary errorElementsNumber does not match")
        self.assertEqual(trainingSessionSummary.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : trainingSession summary testScore does not match")
        self.assertEqual(trainingSessionSummary.comments,fetchedUpdatedTrainingSession.comments,"ERROR : trainingSession summary comments does not match")
        # patch training session
        updatedSaveInterval = int(saveInterval * uniform(2,10))
        updatedMaximumTry = randint(11,20)
        updatedTrainingMaximumErrorRatio = random()
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = TrainingSession(CommonTest.testPerceptronId,saveInterval=updatedSaveInterval,maximumTry=updatedMaximumTry,maximumErrorRatio=updatedTrainingMaximumErrorRatio,comments=updatedComments)
        dumpedPatchData = patchData.dumpToSimpleJson()
        compressedPatchData = compress(dumpedPatchData.encode())
        clientApplication.patch(globalResource,data=compressedPatchData)
        fetchedReupdatedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check patch
        response = clientApplication.get(specificResource)
        jsonTrainingSession = loads(decompress(response.data).decode())
        fetchedPatchedTrainingSession = loadFromDict(jsonTrainingSession)
        expectedShrinkRatio = updatedSaveInterval / saveInterval
        delta = .1
        self.assertNotEqual(fetchedPatchedTrainingSession.saveInterval,fetchedUpdatedTrainingSession.saveInterval,"ERROR : reupdated saveInterval not updated")
        self.assertEqual(fetchedPatchedTrainingSession.saveInterval,updatedSaveInterval,"ERROR : reupdated saveInterval does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedPatchedTrainingSession.maximumTry,updatedMaximumTry,"ERROR : reupdated maximumTry does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedPatchedTrainingSession.maximumTry,updatedMaximumTry,"ERROR : reupdated maximumTry does not match")
        self.assertNotEqual(fetchedPatchedTrainingSession.comments,fetchedUpdatedTrainingSession.comments,"ERROR : patched trainingSession comments not updated")
        self.assertEqual(fetchedPatchedTrainingSession.comments,updatedComments,"ERROR : patched trainingSession comments does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.meanDifferentialErrors) / len(fetchedReupdatedTrainingSessionProgress.meanDifferentialErrors), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated meanDifferentialErrors ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.errorElementsNumbers) / len(fetchedReupdatedTrainingSessionProgress.errorElementsNumbers), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated errorElementsNumbers ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.resets) / len(fetchedReupdatedTrainingSessionProgress.resets), expectedShrinkRatio, delta=delta, msg="ERROR : reupdated resets ratio does not match")
        self.assertEqual(fetchedPatchedTrainingSession.pid,fetchedUpdatedTrainingSession.pid,"ERROR : reupdated pid does not match")
        self.assertEqual(fetchedPatchedTrainingSession.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : reupdated errorMessage does not match")
        self.assertEqual(fetchedPatchedTrainingSession.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : reupdated testScore does not match")
        # delete training session
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonTrainingSession = loads(decompress(response.data).decode())
        self.assertIsNone(jsonTrainingSession,"ERROR : training session not deleted")
        # clean database
        deleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        perceptronDeleteById(CommonTest.cursor,CommonTest.testPerceptronId)
        deleteAllByTrainingSetId(CommonTest.cursor,CommonTest.testTrainingSetId)
        trainingSetDeleteById(CommonTest.cursor,CommonTest.testTrainingSetId)
        pass
    def testSelectDeleteAll(self):
        # initialize random training sessions
        perceptronIds = insertRandomTrainingSessions()
        CommonTest.connection.commit()
        # select IDs
        globalResourceWithWorkspace = globalTrainingSessionUrl(endpoint,workspaceId=CommonTest.testWorkspaceId)
        response = clientApplication.get(globalResourceWithWorkspace)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        clientApplication.delete(globalResourceWithWorkspace)
        # check IDs
        response = clientApplication.get(globalResourceWithWorkspace)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        # clean database
        perceptronDeleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        trainingSetDeleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post, globalResourceRatio, compressedBadTrainingSession)
    def testPatchDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.patch, globalResource, compressedBadTrainingSession)
    def testPatchSaveIntervalError(self):
        # randomize initial training session
        rawGoodTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId)
        insert(CommonTest.cursor,rawGoodTrainingSession,random())
        CommonTest.connection.commit()
        # generate random training session
        rawBadTrainingSession = TrainingSession(CommonTest.testPerceptronId,saveInterval=-1)
        dumpedBadTrainingSession = rawBadTrainingSession.dumpToSimpleJson()
        compressedBadTrainingSession = compress(dumpedBadTrainingSession.encode())
        response = clientApplication.patch(globalResource, data=compressedBadTrainingSession)
        self.assertEqual(response.status_code, 400, "ERROR : Status code not expected")
    pass
pass
