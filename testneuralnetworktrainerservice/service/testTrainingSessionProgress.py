#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworktrainerservice.service.service import endpoint
from json import loads
from neuralnetworkcommon.database.perceptron import deleteById as perceptronDeleteById
from neuralnetworkcommon.database.trainingSet import deleteById as trainingSetDeleteById, deleteAllByWorkspaceId as trainingSetDeleteAllByWorkspaceId
from gzip import compress, decompress
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from testneuralnetworkcommon.commonUtilities import CommonTest, randomTrainingSessionProgress, insertRandomTrainingSessionProgresses
from neuralnetworkcommon.database.trainingElement import deleteAllByTrainingSetId
from neuralnetworkcommon.database.trainingSessionProgress import deleteByPerceptronId, deleteAllByWorkspaceId
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworktrainerservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalTrainingSessionProgressUrl, specificTrainingSessionProgressUrl
# test training session
globalResource = globalTrainingSessionProgressUrl(endpoint)
compressedBadTrainingSessionProgress = compress(randomTrainingSessionProgress('').dumpToSimpleJson().encode())
class testTrainingSessionProgressWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # initialize session progress
        rawInitiaTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        CommonTest.connection.commit()
        dumpedInitiaTrainingSessionProgress = rawInitiaTrainingSessionProgress.dumpToSimpleJson()
        compressedInitiaTrainingSessionProgress = compress(dumpedInitiaTrainingSessionProgress.encode())
        response = clientApplication.post(globalResource,data=compressedInitiaTrainingSessionProgress)
        # check creation
        self.assertEqual(response.status_code,200,"ERROR : training session was not created")
        # read training session progress
        specificResource = specificTrainingSessionProgressUrl(endpoint,CommonTest.testPerceptronId)
        response = clientApplication.get(specificResource)
        jsonTrainingSessionProgress = loads(decompress(response.data).decode())
        fetchedInsertedTrainingSessionProgress = loadFromDict(jsonTrainingSessionProgress)
        # check reading
        self.assertEqual(rawInitiaTrainingSessionProgress,fetchedInsertedTrainingSessionProgress,"ERROR : inserted trainingSessionProgress does not match")
        # update training session progress
        rawNewTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        dumpedNewTrainingSessionProgress = rawNewTrainingSessionProgress.dumpToSimpleJson()
        compressedNewTrainingSessionProgress = compress(dumpedNewTrainingSessionProgress.encode())
        clientApplication.put(globalResource,data=compressedNewTrainingSessionProgress)
        # check update
        response = clientApplication.get(specificResource)
        jsonTrainingSessionProgress = loads(decompress(response.data).decode())
        fetchedUpdatedTrainingSessionProgress = loadFromDict(jsonTrainingSessionProgress)
        self.assertNotEqual(fetchedUpdatedTrainingSessionProgress,fetchedInsertedTrainingSessionProgress,"ERROR : training session not updated")
        self.assertEqual(fetchedUpdatedTrainingSessionProgress,rawNewTrainingSessionProgress,"ERROR : updated trainingSessionProgress does not match")
        # delete training session progress
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonTrainingSessionProgress = loads(decompress(response.data).decode())
        self.assertIsNone(jsonTrainingSessionProgress,"ERROR : training session not deleted")
        # clean database
        deleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        perceptronDeleteById(CommonTest.cursor,CommonTest.testPerceptronId)
        deleteAllByTrainingSetId(CommonTest.cursor,CommonTest.testTrainingSetId)
        trainingSetDeleteById(CommonTest.cursor,CommonTest.testTrainingSetId)
        pass
    def testSelectDeleteAll(self):
        # initialize random training sessions progresses
        perceptronIds = insertRandomTrainingSessionProgresses()
        CommonTest.connection.commit()
        # select IDs
        globalResourceWithWorkspace = globalTrainingSessionProgressUrl(endpoint,CommonTest.testWorkspaceId)
        response = clientApplication.get(globalResourceWithWorkspace)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        clientApplication.delete(globalResourceWithWorkspace)
        # check IDs
        response = clientApplication.get(globalResourceWithWorkspace)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        # clean database
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        trainingSetDeleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post, globalResource, compressedBadTrainingSessionProgress)
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put, globalResource, compressedBadTrainingSessionProgress)
    pass
pass
