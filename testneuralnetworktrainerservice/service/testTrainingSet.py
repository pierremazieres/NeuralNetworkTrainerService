#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.entity.trainingSet.trainingSet import TrainingSet
from random import choice
from neuralnetworktrainerservice.service.service import endpoint
from json import loads
from gzip import compress, decompress
from string import ascii_letters
from testpythoncommontools.service.service import checkRestServerNotDefaultError
from neuralnetworkcommon.database.trainingElement import deleteAllByTrainingSetId
from testneuralnetworkcommon.commonUtilities import CommonTest, insertRandomTrainingElements, randomTrainingSet, insertRandomTrainingSets
from pythoncommontools.objectUtil.POPO import loadFromDict
from testneuralnetworktrainerservice.service.service import TestService, clientApplication
from neuralnetworkcommon.service.service import globalTrainingSetUrl, specificTrainingSetUrl, trainingSetSummaryUrl
# test training set
compressedBadTrainingSet = compress(TrainingSet('', -1).dumpToSimpleJson().encode())
globalResource = globalTrainingSetUrl(endpoint)
class testTrainingSetWS(TestService):
    # test CRUD OK
    def testCrudOK(self):
        # randomize initial training set
        rawInitialTrainingSet = randomTrainingSet()
        CommonTest.connection.commit()
        dumpedInitialTrainingSet = rawInitialTrainingSet.dumpToSimpleJson()
        compressedInitialTrainingSet = compress(dumpedInitialTrainingSet.encode())
        # precheck
        self.assertIsNone(rawInitialTrainingSet.id,"ERROR : trainingSet has id")
        # create training set
        response = clientApplication.post(globalResource,data=compressedInitialTrainingSet)
        trainingSetId = int(decompress(response.data))
        # check creation
        self.assertIsNotNone(trainingSetId,"ERROR : trainingSet has no id")
        rawInitialTrainingSet.id = trainingSetId
        # read training set
        specificResource = specificTrainingSetUrl(endpoint,trainingSetId)
        response = clientApplication.get(specificResource)
        jsonTrainingSet = loads(decompress(response.data).decode())
        fetchedInsertedTrainingSet = loadFromDict(jsonTrainingSet)
        # check reading
        self.assertEqual(rawInitialTrainingSet,fetchedInsertedTrainingSet,"ERROR : inserted trainingSet does not match")
        # update training set
        rawNewTrainingSet = randomTrainingSet(trainingSetId)
        dumpedNewTrainingSet = rawNewTrainingSet.dumpToSimpleJson()
        compressedNewTrainingSet = compress(dumpedNewTrainingSet.encode())
        clientApplication.put(globalResource,data=compressedNewTrainingSet)
        # check update
        response = clientApplication.get(specificResource)
        jsonTrainingSet = loads(decompress(response.data).decode())
        fetchedUpdatedTrainingSet = loadFromDict(jsonTrainingSet)
        self.assertNotEqual(fetchedUpdatedTrainingSet,fetchedInsertedTrainingSet,"ERROR : trainingSet not updated")
        rawNewTrainingSet.id = trainingSetId
        self.assertEqual(fetchedUpdatedTrainingSet,rawNewTrainingSet,"ERROR : updated trainingSet does not match")
        # fill DB summary
        trainingElementNumber = len(insertRandomTrainingElements(trainingSetId))
        CommonTest.connection.commit()
        # summarize training set
        summaryResource = trainingSetSummaryUrl(endpoint,trainingSetId)
        response = clientApplication.get(summaryResource)
        jsonTrainingSetSummary = loads(decompress(response.data).decode())
        trainingSetSummary = loadFromDict(jsonTrainingSetSummary)
        # check summary
        self.assertEqual(trainingSetSummary.id,fetchedUpdatedTrainingSet.id,"ERROR : trainingSet summary id does not match")
        self.assertEqual(trainingSetSummary.workspaceId,fetchedUpdatedTrainingSet.workspaceId,"ERROR : trainingSet summary workspaceId does not match")
        self.assertEqual(trainingSetSummary.comments,fetchedUpdatedTrainingSet.comments,"ERROR : trainingSet summary comments does not match")
        self.assertEqual(trainingSetSummary.trainingElementsNumber,trainingElementNumber,"ERROR : trainingSet summary trainingElementsNumber does not match")
        # clean DB summary
        deleteAllByTrainingSetId(CommonTest.cursor,trainingSetId)
        # patch training set
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        patchData = TrainingSet(trainingSetId,TestService.testWorkspaceId,updatedComments)
        dumpedPatchData = patchData.dumpToSimpleJson()
        compressedPatchData = compress(dumpedPatchData.encode())
        clientApplication.patch(globalResource,data=compressedPatchData)
        CommonTest.connection.commit()
        # check patch
        response = clientApplication.get(specificResource)
        jsonTrainingSet = loads(decompress(response.data).decode())
        fetchedPatchedTrainingSet = loadFromDict(jsonTrainingSet)
        self.assertNotEqual(fetchedPatchedTrainingSet.comments,fetchedUpdatedTrainingSet.comments,"ERROR : patched trainingSet comments not updated")
        self.assertEqual(fetchedPatchedTrainingSet.comments,updatedComments,"ERROR : patched trainingSet comments does not match")
        # delete training set
        clientApplication.delete(specificResource)
        # check deletion
        response = clientApplication.get(specificResource)
        jsonTrainingSet = loads(decompress(response.data).decode())
        self.assertIsNone(jsonTrainingSet,"ERROR : trainingSet not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training sets
        trainingSetsIds=insertRandomTrainingSets()
        CommonTest.connection.commit()
        # select IDs
        globalRessourceWorkspaceId = globalTrainingSetUrl(endpoint,CommonTest.testWorkspaceId)
        response = clientApplication.get(globalRessourceWorkspaceId)
        fetchedIds = loads(decompress(response.data).decode())
        # check IDs
        self.assertTrue(trainingSetsIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        clientApplication.delete(globalRessourceWorkspaceId)
        # check IDs
        response = clientApplication.get(globalRessourceWorkspaceId)
        deletedIds = loads(decompress(response.data).decode())
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test error
    def testPostDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.post, globalResource, compressedBadTrainingSet)
    def testPutDataError(self):
        checkRestServerNotDefaultError(self, clientApplication.put, globalResource, compressedBadTrainingSet)
    pass
pass
