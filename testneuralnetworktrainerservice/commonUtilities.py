# coding=utf-8
# import
from neuralnetworkcommon.database.trainingSession import updatePid, insert as trainingSessionInsert
from neuralnetworkcommon.database.trainingElement import insert as trainingElementInsert
from neuralnetworkcommon.database.layer import insert as layerInsert
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkcommon.entity.trainingElement import TrainingElement
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from random import randint
from neuralnetworktrainerservice.business.trainer import Trainer
from testneuralnetworkcommon.commonUtilities import randomPerceptron, CommonTest, insertRandomTrainingElements
from neuralnetworkcommon.database.perceptron import update
# test utilities
DIGITS_HEIGTH = 5
DIGITS_LENGTH = 3
ROWS_NUMBER = DIGITS_HEIGTH * 5
COLUMNS_NUMBER = DIGITS_LENGTH * 4
PIXELS_NUMBER = ROWS_NUMBER * COLUMNS_NUMBER
def setRandomPerceptronTrainingSetSession():
    # randomize perceptron
    perceptron = randomPerceptron(CommonTest.testPerceptronId)
    update(CommonTest.cursor,perceptron)
    # create training session
    inputLength = randint(75,100)
    outputLength = randint(25,50)
    layer = Layer(CommonTest.testPerceptronId,0,None,None)
    layerInsert(CommonTest.cursor,layer, inputLength, outputLength)
    insertRandomTrainingElements(CommonTest.testTrainingSetId, inputLength, outputLength)
    trainingSession = TrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId)
    trainingSessionInsert(CommonTest.cursor,trainingSession, 0)
    # create and return trainer
    CommonTest.connection.commit()
    trainer = Trainer(CommonTest.testPerceptronId)
    return trainer
'''
INFO : create a usable and simple sequence to train a neural network
 - input : there is a grid on wich we draw bits (0 or 1) on many different space
   a bit is 5*3 picture and the map is a 25*12 picture
 - output : is the picture a 0 or 1 bit ?
   we use a length 2 vector :
    - 0 : (1,0)
    - 1 : (0,1)
'''
def createCompleteDummyTrainingElements():
    # run over all map
    for startRow in range(ROWS_NUMBER - DIGITS_HEIGTH + 1):
        for startColumn in range(COLUMNS_NUMBER - DIGITS_LENGTH + 1):
            # create 0s
            picture = [0] * PIXELS_NUMBER
            startPixel = (startRow * COLUMNS_NUMBER) + startColumn
            endPixel = startPixel + (DIGITS_HEIGTH * COLUMNS_NUMBER)
            for pixelIndex in range(startPixel, endPixel, COLUMNS_NUMBER):
                picture[pixelIndex] = 1
                picture[pixelIndex + DIGITS_LENGTH - 1] = 1
            picture[startPixel + 1] = 1
            picture[pixelIndex + DIGITS_LENGTH - 2] = 1
            trainingElement = TrainingElement(None, CommonTest.testTrainingSetId, picture,[1, 0])
            trainingElementInsert(CommonTest.cursor,trainingElement)
            # create 1s
            picture = [0] * PIXELS_NUMBER
            startPixel = (startRow * COLUMNS_NUMBER) + startColumn
            endPixel = startPixel + (DIGITS_HEIGTH * COLUMNS_NUMBER)
            for pixelIndex in range(startPixel, endPixel, COLUMNS_NUMBER):
                picture[pixelIndex + 1] = 1
            trainingElement = TrainingElement(None, CommonTest.testTrainingSetId, picture,[0, 1])
            trainingElementInsert(CommonTest.cursor,trainingElement)
            pass
        pass
    pass
def prepareTrainer(maximumTry=1e9, testScore=0):
    # randomize perceptron and training set & session
    hiddenLayerLength = 10
    layer0 = Layer(CommonTest.testPerceptronId,0,None,None)
    layerInsert(CommonTest.cursor,layer0, ROWS_NUMBER * COLUMNS_NUMBER, hiddenLayerLength)
    layer1 = Layer(CommonTest.testPerceptronId,1,None,None)
    layerInsert(CommonTest.cursor,layer1, hiddenLayerLength, 2)
    createCompleteDummyTrainingElements()
    initialTrainingSession = TrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId,maximumTry=maximumTry)
    trainingSessionInsert(CommonTest.cursor,initialTrainingSession, testScore)
    pid = randint(1, 1e3)
    updatePid(CommonTest.cursor,CommonTest.testPerceptronId, pid)
    CommonTest.connection.commit()
    # train completely
    trainer = Trainer(CommonTest.testPerceptronId)
    # return usefull elements
    return trainer, initialTrainingSession, pid
pass
