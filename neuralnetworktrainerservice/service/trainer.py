# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.database.trainingSession import updatePid, selectByPerceptronId
from os import sep
from os.path import join, realpath
from pythoncommontools.daemonCommon.daemonCommon import start, stop
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import application, dumpResponse, Action, inputLogFormator, outputLogFormator, responseError, errorLogFormator
from neuralnetworkcommon.service.service import TrainerParameters, TrainerAction
from time import sleep
# constants
currentDirectory = realpath(__file__).rsplit(sep, 1)[0]
# trainer
def startTrainer(cursor,perceptronId):
    # TODO : use a docker container instead of a independant process
    # stop trainer
    stopTrainer(cursor,perceptronId)
    # start trainer
    scriptPath = join(currentDirectory, "..", "business", "trainer.py")
    pid = start("python3", scriptPath, str(perceptronId))
    updatePid(cursor, perceptronId, pid)
    return pid
def stopTrainer(cursor,perceptronId):
    trainingSession = selectByPerceptronId(cursor,perceptronId)
    # TODO : when docker container is done training, delete it instead of killing process
    stop(trainingSession.pid)
    updatePid(cursor,perceptronId, None)
    pass
class Trainer(ConnectionPooledResource):
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( Trainer.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        action = parameters.get(TrainerParameters.ACTION.value)
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            cursor = connection.cursor()
            # select action
            if action:
                start = TrainerAction.START.value
                action = action.lower()
                if (action in {start, TrainerAction.STOP.value}):
                    if action == start:
                        response = startTrainer(cursor,perceptronId)
                        response = dumpResponse(response)
                        # output log
                        message = outputLogFormator(method, resource)
                        application.logger.info(message)
                        pass
                    else:
                        stopTrainer(cursor,perceptronId)
                        pass
                    connection.commit()
                    # WARN : sleep for the OS to finish thread start/stop
                    sleep(1)
                else:
                    raise Exception("Action is invalid")
            else:
                raise Exception("Action is required")
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    pass
pass
