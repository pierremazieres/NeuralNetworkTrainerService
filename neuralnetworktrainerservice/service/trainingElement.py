# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.service.service import CommonParameters, TrainerParameters
from neuralnetworkcommon.database.trainingElement import insert,update,selectAllByTrainingSetIdAndFilter,deleteAllByTrainingSetId,selectById,deleteById
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import application, loadRequest, dumpResponse, Action, inputLogFormator, outputLogFormator, responseError, errorLogFormator
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter
from pythoncommontools.objectUtil.POPO import loadFromDict
from pythoncommontools.service.httpSymbol import quotedStringToArray
# global training element resource
class GlobalTrainingElement(ConnectionPooledResource):
    # create a training element
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalTrainingElement.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingElement = loadRequest(request)
        trainingElement = loadFromDict(dictTrainingElement)
        # insert training element
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert training element
            cursor = connection.cursor()
            insert(cursor,trainingElement)
            connection.commit()
            response = dumpResponse(trainingElement.id)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a training element
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalTrainingElement.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingElement = loadRequest(request)
        trainingElement = loadFromDict(dictTrainingElement)
        # update training element
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update training element
            cursor = connection.cursor()
            update(cursor,trainingElement)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all training elements IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalTrainingElement.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        trainingSetId = parameters[TrainerParameters.TRAININGSET_ID.value] if TrainerParameters.TRAININGSET_ID.value in parameters else None
        perceptronId = parameters[CommonParameters.PERCEPTRON_ID.value] if CommonParameters.PERCEPTRON_ID.value in parameters else None
        expectedOutput = quotedStringToArray(parameters[TrainerParameters.EXPECTED_OUTPUT.value],convertionFunction=float) if TrainerParameters.EXPECTED_OUTPUT.value in parameters else None
        testFilter = parameters[TrainerParameters.TEST_FILTER.value].lower()=="true" if TrainerParameters.TEST_FILTER.value in parameters else None
        errorFilter = parameters[TrainerParameters.ERROR_FILTER.value].lower()=="true" if TrainerParameters.ERROR_FILTER.value in parameters else None
        loadedTrainingElementIds = []
        response = None
        if trainingSetId or perceptronId:
            connection = ConnectionPooledResource.connectionPool.getconn()
            cursor = connection.cursor()
            if trainingSetId:
                loadedTrainingElementIds = selectAllByTrainingSetIdAndFilter(cursor,trainingSetId,expectedOutput)
            else:
                loadedTrainingElementIds = selectAllByPerceptronIdAndFilter(cursor,perceptronId,expectedOutput,testFilter,errorFilter)
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
            response = dumpResponse(loadedTrainingElementIds)
        else:
            response = responseError(500, Exception("You must set a training set or a perceptron ID"))
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete all training elements
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalTrainingElement.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        trainingSetId = parameters[TrainerParameters.TRAININGSET_ID.value] if TrainerParameters.TRAININGSET_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByTrainingSetId(cursor,trainingSetId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        pass
    pass
# specific training element resource
class SpecificTrainingElement(ConnectionPooledResource):
    # select a training element
    def get(self,trainingElementId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( SpecificTrainingElement.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingElement = selectById(cursor,trainingElementId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingElement = None
        # resource process
        if rawTrainingElement:
            jsonTrainingElement = rawTrainingElement.dumpToDict()
        # encode response
        response = dumpResponse(jsonTrainingElement)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a training element
    def delete(self, trainingElementId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificTrainingElement.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteById(cursor,trainingElementId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        pass
    pass
pass
