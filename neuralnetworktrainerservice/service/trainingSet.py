# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.service.service import CommonParameters
from neuralnetworkcommon.database.trainingSet import insert, update, patch, selectAllIdsByWorkspaceId, deleteAllByWorkspaceId, selectById, deleteById, selectSummaryById
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import application, loadRequest, dumpResponse, Action, inputLogFormator, outputLogFormator, responseError, errorLogFormator
from pythoncommontools.objectUtil.POPO import loadFromDict
# global training set resource
class GlobalTrainingSet(ConnectionPooledResource):
    # create a trainingSet
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSet.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSet = loadRequest(request)
        trainingSet = loadFromDict(dictTrainingSet)
        # insert training set
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert training set
            cursor = connection.cursor()
            insert(cursor,trainingSet)
            connection.commit()
            response = dumpResponse(trainingSet.id)
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    pass
    # update a training set
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSet.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSet = loadRequest(request)
        trainingSet = loadFromDict(dictTrainingSet)
        # update trainingSet
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update perceptron
            cursor = connection.cursor()
            update(cursor,trainingSet)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # patch a training set
    def patch(self):
        # input log
        method = Action.PATCH
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSet.patch ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSet = loadRequest(request)
        trainingSet = loadFromDict(dictTrainingSet)
        # patch training set
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            cursor = connection.cursor()
            patch(cursor,trainingSet)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all training sets IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSet.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedTrainingSetIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedTrainingSetIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a training set
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSet.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByWorkspaceId(cursor,workspaceId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific training set resource
class SpecificTrainingSet(ConnectionPooledResource):
    # select a trainingSet
    def get(self,trainingSetId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSet.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingSet = selectById(cursor,trainingSetId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingSet = None
        if rawTrainingSet:
            jsonTrainingSet = rawTrainingSet.dumpToDict()
        # encode response
        response = dumpResponse(jsonTrainingSet)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a training set
    def delete(self,trainingSetId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSet.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteById(cursor,trainingSetId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# training set summary resource
class TrainingSetSummary(ConnectionPooledResource):
    def get(self,trainingSetId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( TrainingSetSummary.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingSetSummary = selectSummaryById(cursor,trainingSetId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingSetSummary = None
        if rawTrainingSetSummary:
            jsonTrainingSetSummary = rawTrainingSetSummary.dumpToDict()
        response = dumpResponse(jsonTrainingSetSummary)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    pass
pass
