# coding=utf-8
# import
from neuralnetworkcommon.service.service import TrainerResourcePathType
from neuralnetworktrainerservice.service.trainer import Trainer
from neuralnetworktrainerservice.service.trainingSession import GlobalTrainingSession, SpecificTrainingSession, TrainingSessionSummary
from neuralnetworktrainerservice.service.trainingSessionProgress import GlobalTrainingSessionProgress, SpecificTrainingSessionProgress
from neuralnetworktrainerservice.service.trainingSet import GlobalTrainingSet, SpecificTrainingSet, TrainingSetSummary
from neuralnetworktrainerservice.service.trainingElement import GlobalTrainingElement, SpecificTrainingElement
from os import environ
from pythoncommontools.service.service import API, application, log
from pythoncommontools.service.httpSymbol import HttpSymbol
# constants
endpoint=environ.get("NEURALNETWORK_TRAINER_SERVICE_ENDPOINT")
logLevel=environ.get("NEURALNETWORK_TRAINER_SERVICE_LOG_LEVEL")
# initialize service
API.add_resource(GlobalTrainingSet, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.GLOBAL_TRAININGSET.value,)))
API.add_resource(SpecificTrainingSet, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.SPECIFIC_TRAININGSET.value,)))
API.add_resource(TrainingSetSummary, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.TRAININGSET_SUMMARY.value,)))
API.add_resource(GlobalTrainingElement, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,)))
API.add_resource(SpecificTrainingElement, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.SPECIFIC_TRAININGELEMENT.value,)))
API.add_resource(GlobalTrainingSession, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,)))
API.add_resource(SpecificTrainingSession, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.SPECIFIC_TRAININGSESSION.value,)))
API.add_resource(GlobalTrainingSessionProgress, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value)))
API.add_resource(SpecificTrainingSessionProgress, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.SPECIFIC_TRAININGSESSIONPROGRESS.value)))
API.add_resource(TrainingSessionSummary, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.TRAININGSESSION_SUMMARY.value)))
API.add_resource(Trainer, HttpSymbol.PATH_SEPARATOR.value.join(('',endpoint,TrainerResourcePathType.TRAINER_EXECUTION.value,)))
# set log
application.logger.setLevel(logLevel)
log.setLevel(logLevel)
