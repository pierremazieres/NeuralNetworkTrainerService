# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.service.service import CommonParameters
from neuralnetworkcommon.database.trainingSessionProgress import insert,update,selectAllIdsByWorkspaceId,deleteAllByWorkspaceId, selectByPerceptronId, deleteByPerceptronId
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import application, loadRequest, dumpResponse, Action, inputLogFormator, outputLogFormator, responseError, errorLogFormator
from pythoncommontools.objectUtil.POPO import loadFromDict
# global training session progress resource
class GlobalTrainingSessionProgress(ConnectionPooledResource):
    # create a training session progress
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSessionProgress.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSessionProgress = loadRequest(request)
        trainingSessionProgress = loadFromDict(dictTrainingSessionProgress)
        # insert training session progress
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert training session progress
            cursor = connection.cursor()
            insert(cursor,trainingSessionProgress)
            connection.commit()
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a training session progress
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSessionProgress.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSessionProgress = loadRequest(request)
        trainingSessionProgress = loadFromDict(dictTrainingSessionProgress)
        # update training session progress
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update training session progress
            cursor = connection.cursor()
            update(cursor,trainingSessionProgress)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all training session progresses (perceptron)IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSessionProgress.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedTrainingSessionProgressIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedTrainingSessionProgressIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete all training session progresses
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSessionProgress.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByWorkspaceId(cursor,workspaceId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific training session progress resource
class SpecificTrainingSessionProgress(ConnectionPooledResource):
    # select a training session progress
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSessionProgress.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingSessionProgress = selectByPerceptronId(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingSessionProgress = None
        # resource process
        if rawTrainingSessionProgress:
            jsonTrainingSessionProgress = rawTrainingSessionProgress.dumpToDict()
        # encode response
        response = dumpResponse(jsonTrainingSessionProgress)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a training session progress
    def delete(self,perceptronId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSessionProgress.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteByPerceptronId(cursor,perceptronId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
