# coding=utf-8
# import
from flask import request
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from inspect import signature
from neuralnetworkcommon.service.service import CommonParameters, TrainerParameters
from neuralnetworkcommon.database.trainingSession import insert,update,selectByPerceptronId,patch,selectAllIdsByWorkspaceId,deleteAllByWorkspaceId,selectSummaryByPerceptronId,deleteByPerceptronId
from pythoncommontools.objectUtil.objectUtil import methodArgsStringRepresentation
from pythoncommontools.service.service import application, loadRequest, dumpResponse, Action, inputLogFormator, outputLogFormator, responseError, errorLogFormator
from pythoncommontools.objectUtil.POPO import loadFromDict
# global training session resource
class GlobalTrainingSession(ConnectionPooledResource):
    # create a training session
    def post(self):
        # input log
        method = Action.POST
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSession.post ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        testRatio = float(parameters[TrainerParameters.TEST_RATIO.value]) if TrainerParameters.TEST_RATIO.value in parameters else None
        dictTrainingSession = loadRequest(request)
        trainingSession = loadFromDict(dictTrainingSession)
        # insert training session
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # insert training session
            cursor = connection.cursor()
            insert(cursor,trainingSession,testRatio)
            connection.commit()
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # update a training session
    def put(self):
        # input log
        method = Action.PUT
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSession.put ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        testRatio = float(parameters[TrainerParameters.TEST_RATIO.value]) if TrainerParameters.TEST_RATIO.value in parameters else None
        dictTrainingSession = loadRequest(request)
        trainingSession = loadFromDict(dictTrainingSession)
        # update training session
        response = None
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # update training session
            cursor = connection.cursor()
            update(cursor,trainingSession,testRatio)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            response = responseError(500,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # patch a training session
    def patch(self):
        # input log
        method = Action.PATCH
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSession.patch ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # parse parameters
        dictTrainingSession = loadRequest(request)
        trainingSession = loadFromDict(dictTrainingSession)
        # patch training session
        response = "If training is in progress, please restart trainer for change to take effect"
        saveIntervalErrorMessage = Exception("HTTP 400 : Bad Request ; save interval can not be decreased")
        connection = ConnectionPooledResource.connectionPool.getconn()
        try:
            # check save interval consistency
            cursor = connection.cursor()
            existingTrainingSession = selectByPerceptronId(cursor,trainingSession.perceptronId)
            if (trainingSession.saveInterval < existingTrainingSession.saveInterval) :
                raise saveIntervalErrorMessage
            # patch training session
            patch(cursor,trainingSession)
            connection.commit()
            # output log
            message = outputLogFormator(method, resource)
            application.logger.info(message)
        except Exception as exception:
            errorCode = 400 if exception==saveIntervalErrorMessage else 500
            response = responseError(errorCode,exception)
            message = errorLogFormator(method, resource, exception)
            application.logger.error(message)
        finally:
            cursor.close()
            ConnectionPooledResource.connectionPool.putconn(connection)
        return response
    # get all training sessions (perceptron)IDs
    def get(self):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSession.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        loadedTrainingSessionIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        response = dumpResponse(loadedTrainingSessionIds)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete all training sessions
    def delete(self):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( GlobalTrainingSession.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        workspaceId = parameters[CommonParameters.WORKSPACE_ID.value] if CommonParameters.WORKSPACE_ID.value in parameters else None
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteAllByWorkspaceId(cursor,workspaceId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# specific training session resource
class SpecificTrainingSession(ConnectionPooledResource):
    # select a training session
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSession.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingSession = selectByPerceptronId(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingSession = None
        # resource process
        if rawTrainingSession:
            jsonTrainingSession = rawTrainingSession.dumpToDict()
        # encode response
        response = dumpResponse(jsonTrainingSession)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    # delete a training session
    def delete(self,perceptronId):
        # input log
        method = Action.DELETE
        resource = methodArgsStringRepresentation( signature( SpecificTrainingSession.delete ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        deleteByPerceptronId(cursor,perceptronId)
        connection.commit()
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
    pass
# training session summary resource
class TrainingSessionSummary(ConnectionPooledResource):
    def get(self,perceptronId):
        # input log
        method = Action.GET
        resource = methodArgsStringRepresentation( signature( TrainingSessionSummary.get ).parameters,locals() )
        parameters = request.args
        message = inputLogFormator(method, resource, parameters)
        application.logger.info(message)
        # resource process
        connection = ConnectionPooledResource.connectionPool.getconn()
        cursor = connection.cursor()
        rawTrainingSessionSummary = selectSummaryByPerceptronId(cursor,perceptronId)
        cursor.close()
        ConnectionPooledResource.connectionPool.putconn(connection)
        jsonTrainingSessionSummary = None
        if rawTrainingSessionSummary:
            jsonTrainingSessionSummary = rawTrainingSessionSummary.dumpToDict()
        response = dumpResponse(jsonTrainingSessionSummary)
        # output log & return
        message = outputLogFormator(method, resource)
        application.logger.info(message)
        return response
    pass
pass
