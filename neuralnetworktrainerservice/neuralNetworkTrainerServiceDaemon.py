#!/usr/bin/env python3
# coding=utf-8
# import
from os import environ
from os.path import basename, extsep
from pythoncommontools.daemonCommon.daemonCommon import startSingleInstance, stopSingleInstance, statusSingleInstance, daemonize
from neuralnetworktrainerservice.service.service import application
import atexit
from signal import signal, SIGTERM
from neuralnetworkcommon.service.connectionPooledResource import customKill
# global initialization
scriptFullPath = __file__
scriptName = basename(__file__).split(extsep)[0]
# custom methods
def customStart():
    # start single instance
    # TODO : set single instance on each port
    startSingleInstance(scriptName)
    # load configuration
    host = environ.get("NEURALNETWORK_TRAINER_SERVICE_HOST")
    port = int(environ.get("NEURALNETWORK_TRAINER_SERVICE_PORT"))
    # start service
    application.run(host, port)
# INFO : connection poll is automatically closed by Postgres framework
@atexit.register
def customStop():
    stopSingleInstance(scriptName)
    pass
def customStatus():
    status = statusSingleInstance(scriptName)
    print(status)
# run script
signal(SIGTERM, customKill)
if __name__ == "__main__" :
    daemonize(customStart,customStop,customStatus)
    pass
pass
