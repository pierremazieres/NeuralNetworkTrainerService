#!/usr/bin/env python3
# coding=utf-8
# import
from argparse import ArgumentParser
from neuralnetworkcommon.database.trainingSession import updateTestScore,updatePid,updateError,selectByPerceptronId
from neuralnetworkcommon.database.trainingSessionProgress import initialize, append
from neuralnetworkcommon.database.trainer import TrainerDB
from neuralnetworkcommon.database.database import connectDatabase
# trainer
class Trainer():
    def trainCompleteSequence(self):
        # assume there is no reset
        reset = False
        # train only if there is a dedicated sequence
        trainingElementsNumber = self.trainerDB.generateTrainingSubSequence()
        if trainingElementsNumber > 0:
            # initialize training sequence
            self.intervalCounter = 0
            meanDifferentialError, errorElementsNumber, reset = self.trainSubSequence()
            initialize(self.cursor,self.perceptronId, meanDifferentialError,errorElementsNumber,reset)
            lastSelectedErrorElementsNumber = errorElementsNumber
            # train step by step over complete sequence
            trainingActualErrorRatio = errorElementsNumber / trainingElementsNumber
            while (trainingActualErrorRatio > self.maximumErrorRatio or lastSelectedErrorElementsNumber>errorElementsNumber) and (not reset):
                meanDifferentialError, errorElementsNumber, reset = self.trainSubSequence()
                trainingActualErrorRatio = errorElementsNumber / trainingElementsNumber
                # prepare new sub training
                if not reset:
                    self.trainerDB.generateTrainingSubSequence()
                    # memorize progress (if needed)
                    if lastSelectedErrorElementsNumber>errorElementsNumber:
                        lastSelectedErrorElementsNumber = errorElementsNumber
            # save progress & perceptron (if needed)
            if not reset:
                # fill report
                append(self.cursor,self.perceptronId, meanDifferentialError, errorElementsNumber,False)
        # commit progress
        self.connection.commit()
        # return reset
        return reset
    def trainSubSequence(self):
        currentTry = 0
        trainingProgress = False
        meanDifferentialError = 0
        errorElementsNumber = 0
        reset = None
        # train as many as necessary
        while (not trainingProgress) and currentTry<self.maximumTry:
            reset = False
            # train once
            meanDifferentialError = self.trainerDB.passForwardBackwardSequence()
            errorElementsNumber = self.trainerDB.countErrorElements()
            # increment counters
            currentTry += 1
            self.intervalCounter += 1
            # manage saving and reset
            if currentTry==self.maximumTry or self.intervalCounter==self.saveInterval:
                # fill report
                reset = (not trainingProgress) and currentTry == self.maximumTry
                append(self.perceptronId, meanDifferentialError, errorElementsNumber,reset)
                self.intervalCounter = 0
            trainingProgress = self.trainerDB.isPartiallyTrained()
        # INFO : sometime (ie save interval < 0, reset is mandatory)
        if reset is None:
            reset = True
        return meanDifferentialError, errorElementsNumber, reset
    def run(self):
        raisedException = None
        try:
            # execute complete training
            updateTestScore(self.cursor,self.perceptronId, None)
            # INFO : commit will be done at the end of complete sequence training
            reset = True
            while reset:
                reset = self.trainCompleteSequence()
            self.trainerDB.computeTestScore()
            # INFO : commit done in finally block
        except Exception as exception :
            raisedException = exception
        finally:
            # clean trainer DB
            self.trainerDB.cleanup()
            # erase PID
            updatePid(self.cursor,self.perceptronId,None)
            # write exception (if needed)
            if raisedException :
                updateError(self.cursor,self.perceptronId,str(raisedException))
            # close connection
            self.connection.commit()
            self.cursor.close()
            self.connection.close()
            # raise exception (if needed)
            if raisedException:
                raise raisedException
        pass
    pass
    #constructors
    def __init__(self, perceptronId):
        # INFO : trainer if a autonomous thread, so it need is own connection (not attached to connection pool)
        self.connection = connectDatabase()
        self.cursor = self.connection.cursor()
        self.perceptronId = perceptronId
        self.trainerDB = TrainerDB(perceptronId,self.connection,self.cursor)
        trainingSession = selectByPerceptronId(self.cursor,perceptronId)
        self.saveInterval = trainingSession.saveInterval
        self.maximumTry = trainingSession.maximumTry
        self.maximumErrorRatio = trainingSession.maximumErrorRatio
        pass
    pass
# auto run trainer if started as main script
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("perceptronId", type=int)
    args = parser.parse_args()
    perceptronId = args.perceptronId
    trainer = Trainer(perceptronId)
    trainer.run()
pass
