#!/usr/bin/env python3
# coding=utf-8
# import
from setuptools import setup, find_packages
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
# define setup parameters
setup(
    name="NeuralNetworkTrainerService",
    version="0.0.3",
    description="Neural network trainer service",
    packages=find_packages(),
    install_requires=["neuralnetworkcommon","psycopg2","psycopg2-binary","flask_restful","flask"],
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
